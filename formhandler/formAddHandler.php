<!DOCTYPE html>
<html>
    <?php

        include_once "../database/sqlStatements.php";

        // Map that return specific attribute depending on item type
        $typeMap = array(
            "DVD Disc" => "Size: " . $_POST["size"] . " MB",
            "Book" => "Weight: " .$_POST["weight"] . " KG",
            "Furniture" => "Dimensions: " . $_POST["furHeight"] . "x" . $_POST["furWidth"] .  "x" . $_POST["furLength"]
            
        );

        // Getting item informtaion through $_POST 
        // and setting binded paramters from "dbManager.php"
        $id = $_POST["id"];
        $name = $_POST["name"];
        $price = $_POST["price"];
        $type = $_POST["types"];
        $attribute = $typeMap[$type];
        echo $type;
        echo $attribute;

        // Inserting data into database
        $GLOBALS["insertStatement"]->execute();

        // returning to main page
        header("Location: ../productList/productList.php");
    ?>

</html>
