<?php

    include_once "dbConnection.php";

    // SQL prepared statements

    // Statement to insert data into the database
    $GLOBALS["insertStatement"] = $GLOBALS["connection"]->prepare("INSERT INTO products VALUES (?, ?, ?, ?, ?)");
    $GLOBALS["insertStatement"]->bind_param("ssiss", $id, $name, $price, $type, $attribute);


    // Statement to remove data from the database
    $GLOBALS["removeStatement"] = $GLOBALS["connection"]->prepare("DELETE FROM products WHERE productID = ?");
    $GLOBALS["removeStatement"]->bind_param("s", $id);
    
?>