<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="productList.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="../../product-manager/formHandler/formDeleteHandler.php" method="post">   
        <header>
                <h1 id="product-title">Product List</h1>
                <div id="controller" >
                    <input type="submit" value="Delete Selected">    
                    <a href="../productAdd/productAdd.html" id="addItem" >Add Item</a>
                </div>
        </header>

        <main id="productList">
            <!-- php code to dynamically fetch data from database and update DOM -->
            <?php
                include_once "../database/sqlStatements.php";

                $sql = "SELECT * FROM products";
                $result = $GLOBALS["connection"]->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        $id = $row["productID"];
                        $name = $row["productName"];
                        $price = $row["productPrice"];
                        $attribute = $row["productSpecialAttribute"];
            ?>
                        <!-- Item Tempalte code -->
                        <div class='item'> 
                            <input  class="checkbox" name="checkbox[]" type='checkbox' value="<?="{$id}"?>"><br>
                            <p><?=$id?></p>
                            <p><?=$name?></p>
                            <p><?=$price?>$</p>
                            <p><?=$attribute?></p>
                        </div>

            <?php
                    }
                }
            ?>
        </main>
    </form>
</body>
</html>