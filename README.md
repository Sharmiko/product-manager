###
    To start the web application please use index.php file.

    The following queries were used to create the database and the table.

    CREATE DATABASE products;

    CREATE TABLE products (
        productID VARCHAR(8) NOT NULL UNIQUE,
        productName VARCHAR(30) NOT NULL,
        productPrice INT(6) NOT NULL,
        productType VARCHAR(30) NOT NULL,
        productSpecialAttribute VARCHAR(30) NOT NULL
    );

###