const dvdCode = "Disk Size:<br>" + 
                "<input type=\"text\" name=\"size\">";

const bookCode = "Book Weight:<br>" +
                 "<input type=\"text\" name=\"weight\">";

const furnitureCode = "<h3>Furniture Dimensions</h3>" +
                      "Height<br><input type=\"text\" name=\"furHeight\">" + 
                      "Width<br><input type=\"text\" name=\"furWidth\">" +
                      "Length<br><input type=\"text\" name=\"furLength\">";


// Map of item types that returns proper code 
const itemTypes = {
    "DVD Disc": dvdCode,
    "Book": bookCode,
    "Furniture": furnitureCode
}


function typeSwitchHandler() {
    // Get node where type code should be inserted
    var typePlace = document.querySelector("#typePlace");
    while (typePlace.firstChild) { // check if it contains children nodes and remove them
        typePlace.removeChild(typePlace.firstChild);
    }
    // get type of item
    var type = document.querySelector("#types");
    var stringType = type.options[type.selectedIndex].value;    

    // Set type title 
    var typeTitle = document.querySelector("#typeTitle");
    typeTitle.innerHTML = stringType;

    // Append proper item type code to insert place
    typePlace.insertAdjacentHTML("afterbegin", itemTypes[stringType]);
}



