<?php
/*

This DataBase use following structure of the table:
CREATE TABLE products (
    productID VARCHAR(8) NOT NULL UNIQUE,
    productName VARCHAR(30) NOT NULL,
    productPrice INT(6) NOT NULL,
    productType VARCHAR(30) NOT NULL,
    productSpecialAttribute VARCHAR(30) NOT NULL
)

This database was create in phpmyadmin
and connection was establish to it.

*/

$dbServer = "localhost";
$dbUserName = "root";
$dbPassword = "";
$dbName = "products";
  
  
$GLOBALS["connection"] = new mysqli($dbServer, $dbUserName, $dbPassword, $dbName);

?>