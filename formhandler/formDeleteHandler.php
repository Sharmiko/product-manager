<!DOCTYPE HTML>
<html>
<?php

include_once "../database/sqlStatements.php";

// If user selects no checkboxes do nothing
if (count($_POST) == 0 ){ 
        header("Location: ../../product-manager/productList/productList.php"); 
}
else { // else get checkbox values through $_POST and remove data from database
    $checkboxes = $_POST["checkbox"];
    foreach ($checkboxes as $checkbox) {
        $id = $checkbox;
        $GLOBALS["removeStatement"]->execute();
    }
    header("Location: ../../product-manager/productList/productList.php"); 
}


?>

</html>
